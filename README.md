# Firebase With Codeigniter

Integrasi Firebase dengan Codeigniter 3.

## Instalasi
1. Pastikan komputer telah terinstall [Composer](https://getcomposer.org/).
2. Download atau clone repository ini.
3. Buka terminal dari direktory project yang sudah di download/clone kemudian jalankan perintah.

```bash
composer require kreait/firebase-php
```
4. Jalankan perintah dibawah untuk mengunduh library yang dibutuhkan.
```bash
composer install
```
5. Masukkan file konfigurasi dari project firebase yang berformat .json kedalam folder "application/config".
6. Buka file "application/config/firebase.php" kemudian masukan lokasi file konfigurasi pada langkah sebelumnya menjadi seperti ini

```bash
$config['firebase_app_key'] = __DIR__ . '/../config/nama-file-konfigurasi.json';
```

## Penggunaan
1. Import library CloudMessage pada controller.
```
<?php
use Kreait\Firebase\Messaging\CloudMessage;

class Messaging extends CI_Controller {
...
```
2. Inisialisasi Firebase
```
$firebase = $this->firebase->init();
$messaging = $firebase->createMessaging();
```

3. Kirim pesan kepada device secara spesifik menggunakan token
```
$notification = [
                   "body"=>"Ini contoh notifikasi",
                   "title"=>"Halo"
                ]; //Harus ada atribut 'body' dan 'title'
$data = ["jenis"=>"notifikasi"];


$message = CloudMessage::withTarget('token', $deviceToken)
	->withNotification($notification)
        ->withData($data);
$messaging->send($message);
```
## Catatan
Untuk contoh penggunaan lebih lengkap dapat kunjungi [Firebase.PHP](https://firebase-php.readthedocs.io/en/stable/cloud-messaging.html) serta [codeigniter-firebase-library](https://github.com/ideagital/codeigniter-firebase-library)
