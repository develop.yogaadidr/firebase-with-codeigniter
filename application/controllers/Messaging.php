<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Kreait\Firebase\Messaging\CloudMessage;

class Messaging extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function send()
	{
		$firebase = $this->firebase->init();
		$messaging = $firebase->createMessaging();

		$deviceToken = 'exrNpTPaTZmXUYNff8hfng:APA91bHSgBsZfvMx2xaV6cx1vzbwkycBQn7FAAajvy9-i6CAI0mBgr_ywZOIMK_h7vdhOErdPxhmI_XASX09QezdX9x6JbeUPblDP6lABEFKa2D83l-Ed__EEY0vJKyBN_Czk0LOsu5p';
		$notification = [
            "body"=>"Ini contoh notifikasi",
            "title"=>"Halo"
        ]; //Harus ada atribut 'body' dan 'title'
		$data = ["Nama"=>"Yoga Adi"];
		$message = CloudMessage::withTarget('token', $deviceToken) //Mengirim ke perorangan
		->withNotification($notification)
        ->withData($data);
        
		$messaging->send($message);
        echo "Pesan Terkirim";
	}
}
